package tp04;

import tpQU.tp04.KeyboardException;
import tpQU.tp04.LocalKeyboard;

/**
 * ex2
 */
public class Ex2 {
    public static int[] tab = {17, 12, 15, 38, 29, 157, 89, -22, 0, 5};

    public static int division(int index, int divisor) throws KeyboardException{
        return tab[index]/divisor;
    }

    public static void statement() {
        int x=-1, y = 0;
        try {
            x = LocalKeyboard.readInt("Write the index of the integer to divide: ");
        } catch (KeyboardException e) {
            System.out.println("Erreur dans la saisie du x");
        }
        try {
            y = LocalKeyboard.readInt("Write the divisor: ");
        } catch (KeyboardException e) {
            System.out.println("Erreur dans la saisie du y");
        }
        try {
            System.out.println("The result is: " + division(x,y));
        } catch (ArithmeticException e) {
            System.out.println("Erreur math, division par zéro");
        } catch (NullPointerException e){
            System.out.println("NullPointerException");
        } catch (IndexOutOfBoundsException e){
            System.out.println("La case pointée n'existe pas");

        }
    }
}