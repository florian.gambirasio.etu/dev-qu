package tp04;

/**
 * Thing
 */
public class Thing {
    private String name;
    private int quantity;
    private Container container;

    public Thing(String name, int quantity, Container container) {
        this.name = name;
        if(quantity > container.getCapacity()) this.quantity = 0;
        else this.quantity = quantity;
        this.container = container;
    }

    private void setQuantity(int quantity) throws ExtendedException{
        if(quantity > container.getCapacity()) throw new ExtendedException();
        else this.quantity = quantity;
    }

    private boolean transferToLargerContainer(){
        if(this.container == Container.BLISTER) {this.container = Container.BOX;return true;}
        if(this.container == Container.BOX) {this.container = Container.CRATE;return true;}
        return false;
    }

    public boolean add(int quantity){
        try{
            setQuantity(this.quantity + quantity);
        } catch (ExtendedException e){
            if(transferToLargerContainer())this.add(quantity);
            else {System.out.println("Impossible addition !");return false;}
        }
        return true;
    }

    @Override
    public String toString() {
        return "Thing [name=" + name + ", quantity=" + quantity + ", container=" + container + "]";
    }

    


}