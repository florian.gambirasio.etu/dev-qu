package tp04;

public enum Country {
    FRANCE("FR"), GERMANY("GE"), RUSSIA("RS"), SWEDEN("SE"), AUSTRIA("AT"), ITALY("IT");

    private String vCourte;

    private Country(String vCourte) {
        this.vCourte = vCourte;
    }

    public String getvCourte() {
        return vCourte;
    }
    
}