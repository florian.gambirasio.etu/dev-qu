package tp04;

enum Container{
    BLISTER(1), BOX(10), CRATE(50);

    private int capacity;

    private Container(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }
}