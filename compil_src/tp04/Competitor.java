package tp04;

/**
 * Competitor
 */
public class Competitor {
    private static int cpt;
    private int id;
    private final String NAME;
    private Country nationality;
    private Team team;

    public Competitor(String name, Country nationality, Team team) {
        this.id = Competitor.cpt++;
        this.NAME = name;
        this.nationality = nationality;
        this.team = team;
    }

    public String toString(){
        return ""+this.id+"-"+this.NAME+"("+this.nationality.getvCourte()+") -> "+this.team;
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return NAME;
    }

    public Country getNationality() {
        return nationality;
    }

    public Team getTeam() {
        return team;
    }

    public boolean isFrom(Country c){
        return this.nationality == c;
    }

    public boolean isFrom(Team t){
        return this.team == t;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((NAME == null) ? 0 : NAME.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Competitor other = (Competitor) obj;
        if (id != other.id)
            return false;
        if (NAME == null) {
            if (other.NAME != null)
                return false;
        } else if (!NAME.equals(other.NAME))
            return false;
        return true;
    }

    public static void resetCpt(){Competitor.cpt = 0;}
}