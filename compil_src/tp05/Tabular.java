package tp05;

import java.util.Arrays;

/**
 * Tabular
 */
public class Tabular {

    private Number[] tab;

    public Tabular(int size){
        this.tab = new Number[size];
        for(int i = 0; i < tab.length; i ++){
            tab[i] = 0;
        }
    }

    public void set(int i, Number number){
        if(i < tab.length){
            tab[i] = number;
        }
    }

    public Number max(){
        Number max = tab[0];
        for(Number n : tab){
            if(n.doubleValue() > max.doubleValue()) max = n;
        }
        return max;
    }

    @Override
    public String toString() {
        return "tab =" + Arrays.toString(tab);
    }

    
}