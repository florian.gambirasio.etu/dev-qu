package tp05.Ex2;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Element
 */
public class Element{
    private String name;

    protected ArrayList<Element> components;

    public Element(String name) {
        this.name = name;
        this.components = new ArrayList<Element>();
    }

    public void add(Element e){
        this.components.add(e);
    }

    public void addAll(Collection<? extends Element> ce){
        components.addAll(ce);
    }

    public void remove(Element e){
        this.components.remove(e);
    }

    public String getName(){
        return this.name;
    }

    public String toString(String tab){
        String res = tab+name+":\n";
        for(Element e : components){
            res += tab+e;
        }
        return res;
    }

}