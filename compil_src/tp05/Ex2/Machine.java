package tp05.Ex2;

/**
 * Machine
 */
public class Machine extends Element{

    public Machine(String name) {
        super(name);
    }

    public void add(Element e){
        super.add(e);
    }

    public void remove(Element e) {
        super.remove(e);
    }

    public String toString(){
        String res = this.getName()+":\n";
        String tab = "";
        Piece test = new Piece("test", 0);
        for(Element e : this.components){
            if(test.getClass() == e.getClass()) res += e;
            else {
                tab += "  ";
                res += e.toString(tab);
            }
        }
        return res;
    }
    
}