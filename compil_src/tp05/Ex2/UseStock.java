package tp05.Ex2;

/**
 * UseStock
 */
public class UseStock {

    public static void main(String[] args) {
        Machine listeCompo = new Machine("Poste de Alice");
        listeCompo.add(new Piece("écran", 1340));
        listeCompo.add(new Piece("clavier", 1934));
        listeCompo.add(new Piece("souris", 2805));

        Element uc = new Element("unité centrale");
        uc.add(new Piece("carte mère", 1569));
        uc.add(new Piece("extension mémoire", 289));
        uc.add(new Piece("disque scsi", 299));
        listeCompo.add(uc);

        Element cg = new Element("carte graphique");
        cg.add(new Piece("extension mémoire", 2879));
        cg.add(new Piece("processeur graphique", 7289));
        listeCompo.add(cg);

        listeCompo.add(new Piece("webcam", 2222));

        System.out.println(listeCompo);
    }
    
}