package tp05.Ex2;

/**
 * Piece
 */
public class Piece extends Element{
    private int num;

    public Piece(String name, int num){
        super(name);
        this.num = num;
    }

    public int getNum(){
        return num;
    }

    public String toString(){
        return this.getName()+"(N."+this.num+")\n";
    }
}