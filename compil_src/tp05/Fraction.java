package tp05;

/**
 * Fraction
 */
public class Fraction extends Number {
    private int num;
    private int denom;

    public Fraction(int num, int denom){
        this.num = num;
        this.denom = denom;
    }

    

    @Override
    public String toString() {
        return num + "/" + denom;
    }



    @Override
    public double doubleValue() {
        return (double) num / (double) denom;
    }

    @Override
    public float floatValue() {
        return (float) num / (float) denom;
    }

    @Override
    public int intValue() {
        return num/denom;
    }

    @Override
    public long longValue() {
        return (long) num / (long) denom;
    }

    
}

