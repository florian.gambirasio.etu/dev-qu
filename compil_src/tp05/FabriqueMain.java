package tp05;

/**
 * FabriqueMain
 */
public class FabriqueMain {

    public static void main(String[] args) {
        Tabular tab = new Tabular(5);
        tab.set(4, NumberFactory.number(5,4));
        System.out.println(tab);
        tab.set(0, NumberFactory.number(6));
        tab.set(3, NumberFactory.number(3.14));
        tab.set(2, NumberFactory.number(3144559));
        tab.set(1, NumberFactory.number(0.9999999999999));
        System.out.println(tab.max());
        System.out.println(NumberFactory.number("6"));
        System.out.println(NumberFactory.number("3.14"));
        System.out.println(NumberFactory.number("steak"));
        System.out.println(tab);
    }
}