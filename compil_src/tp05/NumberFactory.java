package tp05;

/**
 * NumberFactory
 */
public class NumberFactory {

    public static Number number(int n){
        return n;
    }

    public static Number number(long n){
        return n;
    }

    public static Number number(float n){
        return n;
    }

    public static Number number(double n){
        return n;
    }

    public static Number number(int num, int denom){
        return new Fraction(num, denom);
    }

    public static Number number(String n){
        try{
            Integer i = Integer.parseInt(n);
            return i;
        } catch(NumberFormatException e){
            try {
                Double d = Double.parseDouble(n);
                return d;
            } catch (NumberFormatException e2) {
                System.out.println(n + " n'est pas un nombre");
            }
        }
        return 0;
    }
}