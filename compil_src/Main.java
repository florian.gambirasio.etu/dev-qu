import controlepkg1.A;
import controlepkg2.B;
import controlepkg2.sspkg.C;
import controlpkg1.D;

class Main{
    public static void main(String[] args){
        A a = new A("42");
        B b = new B();
        C c = new C("42",3);
        D d = new D();
        System.out.println(a +" "+ b +" "+ c +" "+ d.getSomeText());
    }
}